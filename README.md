## Project PetClinic to Phoebus Infrastructure Documentation 
Este desafio está divido em dois projetos no GitLab:

- [petclinic-infra](https://gitlab.com/mgaabriellima/terraform): Corresponde ao projeto de criação da infraestrutura.
- [petclinic-app](https://gitlab.com/mgaabriellima/petclinic-app): Corresponde ao projeto de deploy da aplicação PetClinic. 

### Objetivo 
- Desenvolver o código necessário para fazer o deploy na AWS da aplicação Petclinic com provisionamento da infraestrutura necessária e automação de CI/CD tanto para a infraestrutura quanto para a aplicação.

### Requisitos
- IaC com Terraform.
- CI da Infraestrutura

### Recursos
- AWS
- Terraform 
- Docker (Docker Hub)
- Gitlab Repo + CI/CD (Infra e App)

## Arquiteura do Projeto 
A arquitetura desse projeto consiste em utilizar o terraform para criar uma infraestrutura na AWS e utilizar o Gitlab CI/CD para criar uma pipeline de integração contínua. 

![alt text](/img/arquitetura-petclinic.png "Arquitetura")

| Recursos | Detalhes |
| ---  | ---     |
| Terraform + AWS| - Security Group<br>- Elastic IP<br> - EC2: Amazon Linux (t2.micro)<br>- S3  |
| Container Docker| - Imagem padrão: hashicorp/terraform<br>- Entrypoint: /bin/terraform|
| GitLab CI/CD |Stages:<br>- Validade<br>- Plan<br>- Apply<br>- Destroy<br>|

## Detalhamento do Projeto
### Terraform
##### O terrform está divido basicamente em 4 arquivos: 
- **main.tf**: Arquivo que define o provider aws, define o backend para o s3 com o objetivo de guardar os estados do terraform "terraform.tfstate", define a instância ec2 e seu elastic ip. 
- **security-group.ft**: Arquivo que define a criação do security group e suas regras de ingress e egress. 
- **data.tf**: Arquivo que define os data sources com o objetivo de buscar informações na estrutura aws como "vpc defaut" e "subnet default". 
- **variables.tf** Arquivo que define as variáveis utilizadas na criação da instância ec2 como "intance name " e "instance type".

### Container Docker
##### Criação da imagem base para CI do Terraform
**Dockerfile-ci**
Foi criado uma imagem para CI com a imagem base do hashicorp + aws cli, com EntryPoint para o binário do Terraform. 

### GitLab CI/CD 
##### Criação da pipeline e stages de CI
**.gitlab-ci.yml**
Utilizado para criação dos stages de CI da infraestrutura dividadas em `validate`, `plan`, `apply`, `destroy`.

Os stages **apply** e **destroy** foram configurados para executar manualmente por meio de uma issue ou merge request. 

### Considerações
O desafio proposto me rendeu bastante aprendizado e foi bem desafiador concluí-lo. Foquei em entregar o laboratório completo com os recursos essenciais definidos como requisito. Desta forma infelizmente não foi possível refinar os recursos e adicionar complexidade ao ambiente proposto como: a adição do SSM para login seguro ao AWS EC2, criação de toda rede (VPC, IGW, NAT-GW), entre outros serviços. 




