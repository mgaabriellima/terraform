#Criação dos data sources para buscar informações na infraestrutura AWS
data "aws_subnet" "app" {
  filter {
    name   = "tag:Name"
    values = ["sub-default-a"]
  }
}

data "aws_vpc" "app" {
  filter {
    name   = "tag:Name"
    values = ["Default"]
  }
}

data "aws_ami" "ami" {
  most_recent = true
  owners = [ "amazon" ]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}