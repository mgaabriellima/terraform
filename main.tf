#Definindo o provider AWS
provider "aws" {
  region = "us-east-2"
}

#Configurando o backend tfstate para o bucket S3
terraform {
  backend "s3" {
    bucket  = "desafio-terraform-state-backend"
    key     = "desafio/terraform-desafio.tfstate"
    region  = "us-east-2"
    encrypt = true
  }
}

#Criação da instância ec2 com data sources e variables
resource "aws_instance" "server-petclinic" {
  ami                  = data.aws_ami.ami.id
  instance_type        = var.inst_type
  key_name             = "AWS-Keys"
  subnet_id            = data.aws_subnet.app.id
  security_groups      = ["${aws_security_group.sg-petclinic.id}"]

  tags        = var.tags
  volume_tags = var.tags

  user_data = <<EOF
		#! /bin/bash
    sudo yum install -y docker
    systemctl enable amazon-ssm-agent && systemctl start amazon-ssm-agent
    systemctl enable docker && systemctl start docker
    docker container run -d -p 80:8080 mgabriellima/devops-projects:app-petclinic
	EOF
}


#Adição do elastic ip para acesso externo
resource "aws_eip" "ip" {
  vpc      = true
  instance = aws_instance.server-petclinic.id
}
