#Criação do security group e suas regras de ingress e egress
resource "aws_security_group" "sg-petclinic" {

  name   = var.inst_name
  vpc_id = data.aws_vpc.app.id

  egress {
    from_port = 0
    to_port   = 0

    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.inst_name
  }
}

resource "aws_security_group_rule" "ingress_vm_pet_ssh" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  description       = "SSH VM PetClinic"
  security_group_id = aws_security_group.sg-petclinic.id
  type              = "ingress"
}

resource "aws_security_group_rule" "ingress_vm_pet_app" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  description       = "Port 80 APP PetClinic"
  security_group_id = aws_security_group.sg-petclinic.id
  type              = "ingress"
}
