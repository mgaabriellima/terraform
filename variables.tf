# Definição das variáveis
variable "inst_region" {
  type    = string 
  default = "us-east-2"
}


variable "inst_type" {
  type    = string
  default = "t2.micro"
}

variable "inst_name" {
  type    = string
  default = "srv-petclinic"
}

variable "tags" {
  description = "Mapa (Map) de tags para os resources."
  type        = map(any)
  default = {
    Name         = "LAB-Desafio"
    Departamento = "Devops"
  }
}
